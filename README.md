# Worley noise

[![Documentation](https://img.shields.io/badge/documentation-online-blue.svg?style=for-the-badge)](https://docs.rs/worley-noise/)
[![Crates.io](https://img.shields.io/crates/v/worley-noise.svg?style=for-the-badge)](https://crates.io/crates/worley-noise)

---

## What is this

[This paper](http://www.rhythmiccanvas.com/research/papers/worley.pdf) explains it pretty well

---

![Worley noise](noise_1.png)
![Worley noise](noise_2.png)
![Worley noise](noise_3.png)
![Worley noise](noise_4.png)

---

## Using it
```
worley-noise = "*"
```

Check the examples directory!

---

## Running the examples
```
cargo run --release --example color_noise
cargo run --release --example animated_color_noise
```

---

## WebAssembly
If compiled for the web (to disable multithreading):
```
worley-noise = { version = "*", features = ["web"] }
```
[Here](https://gitlab.com/Sogomn/webworley) is an example project, [here](https://sogomn.gitlab.io/noise/) is the result.

---

Always open for feedback and questions!
