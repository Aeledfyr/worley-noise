use criterion::{
	Criterion,
	criterion_group,
	criterion_main
};
use worley_noise::WorleyNoise;

const WIDTH: u32 = 50;
const HEIGHT: u32 = 50;
const DEPTH: u32 = 50;

fn create(c: &mut Criterion) {
	c.bench_function("Create", |b| {
		b.iter(|| WorleyNoise::new());
	});
}

fn sample(c: &mut Criterion) {
	let capacity = (WIDTH * HEIGHT * DEPTH) as usize;
	let noise = WorleyNoise::with_cache_capacity(capacity);
	let mut points = Vec::with_capacity(capacity);
	
	for x in 0 .. WIDTH {
		for y in 0 .. HEIGHT {
			for z in 0 .. DEPTH {
				points.push((x as f64, y as f64, z as f64));
			}
		}
	}
	
	c.bench_function("Sample 50x50x50", move |b| {
		b.iter(|| noise.values_3d(&points));
	});
}

fn sample_adjacent(c: &mut Criterion) {
	let capacity = (WIDTH * HEIGHT * DEPTH) as usize;
	let noise = WorleyNoise::with_cache_capacity(capacity);
	let start = (0.0, 0.0, 0.0);
	let end = (WIDTH as f64, HEIGHT as f64, DEPTH as f64);
	let sample_cnt = (WIDTH, HEIGHT, DEPTH);
	
	c.bench_function("Sample adjacent 50x50x50", move |b| {
		b.iter(|| noise.values_3d_range(start, end, sample_cnt));
	});
}

fn permutate(c: &mut Criterion) {
	let mut noise = WorleyNoise::new();
	
	c.bench_function("Permutate", move |b| {
		b.iter(|| noise.permutate(10));
	});
}

criterion_group!(benches, create, sample, sample_adjacent, permutate);
criterion_main!(benches);
